# Videoprojekt

## Installation

```bash
# OpenCV installieren
pip install -r requirements.txt
```

## Usage

```bash
# Overlay from image and watermark
python src/main.py sample/image.jpg sample/watermark.png
```
