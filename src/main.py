from enum import Enum
from pathlib import Path
from cv2 import (
    IMREAD_UNCHANGED,
    destroyAllWindows,
    imread,
    imshow,
    resize,
    waitKey,
    imwrite,
    getTextSize,
    putText,
    FONT_HERSHEY_SIMPLEX,
    FILLED,
)
from numpy import zeros, uint8
from cv2.typing import MatLike
from argparse import ArgumentParser


class Position(Enum):
    TopLeft = "TopLeft"
    BottomLeft = "BottomLeft"
    TopRight = "TopRight"
    BottomRight = "BottomRight"
    Middle = "Middle"

    def __str__(self):
        return self.value


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("background", type=Path, help="path to the background image")
    watermark = parser.add_mutually_exclusive_group(required=True)
    watermark.add_argument("--watermark", type=Path, help="path to the watermark image")
    watermark.add_argument("--text", type=str, help="watermark text content")
    parser.add_argument(
        "position",
        type=Position,
        choices=list(Position),
        help="select the watermark position on the image",
    )
    parser.add_argument("--margin", type=int, required=False, default=0)
    parser.add_argument(
        "--scale",
        type=float,
        required=False,
        default=None,
        help="scaling factor for the watermark",
    )
    parser.add_argument("--opacity", type=float, required=False, default=None)
    parser.add_argument("--output", type=Path, required=False, default=None)
    return parser.parse_args()


def apply_transparency(image: MatLike, transparency: float):
    (_, _, channels) = image.shape
    if channels <= 3:
        raise Exception("Watermark has no alpha channel!")
    image[:, :, 3] = transparency * image[:, :, 3]
    return image


def apply_scale(image: MatLike, scale: float):
    (w_height, w_width, _) = image.shape
    w_width = int(scale * w_width)
    w_height = int(scale * w_height)
    return resize(image, (w_width, w_height))


def overlay(
    background: MatLike, watermark: MatLike, position: Position, margin: int = 0
):
    if margin < 0:
        raise Exception("Margin must not be negative!")
    (w_height, w_width, w_channels) = watermark.shape
    if w_channels <= 3:
        raise Exception("Watermark has no alpha channel!")

    (b_height, b_width, b_channels) = background.shape

    # Resize image if it is too large to fit in the margin
    c_height, c_width = b_height - 2 * margin, b_width - 2 * margin
    do_resize = False
    if w_height > c_height:
        factor = w_height / c_height
        print(
            f"Warning: watermark is too high for the image, scaling down by {factor}!"
        )
        w_height /= factor
        w_width /= factor
        do_resize = True

    if w_width > c_width:
        factor = w_width / c_width
        print(
            f"Warning: watermark is too wide for the image, scaling down by {factor}!"
        )
        w_height /= factor
        w_width /= factor
        do_resize = True

    if do_resize:
        w_height = int(w_height)
        w_width = int(w_width)
        watermark = resize(watermark, (w_width, w_height))

    w_alpha = watermark[:, :, 3] / 255.0  # Alpha Maske des Wasserzeichens
    w_negative = 1.0 - w_alpha  # Negativmaske

    y_start, x_start = (0, 0)

    match position:
        case Position.TopLeft:
            y_start, x_start = (0 + margin, 0 + margin)
        case Position.TopRight:
            y_start, x_start = (0 + margin, b_width - w_width - margin)
        case Position.BottomLeft:
            y_start, x_start = (b_height - w_height - margin, margin)
        case Position.BottomRight:
            y_start, x_start = (
                b_height - w_height - margin,
                b_width - w_width - margin,
            )
        case Position.Middle:
            y_start, x_start = (
                b_height // 2 - w_height // 2,
                b_width // 2 - w_width // 2,
            )
        case other:
            raise Exception(f"Unknown position '{other}'!")

    y_end = y_start + w_height
    x_end = x_start + w_width

    for channel in range(0, b_channels):
        background[y_start:y_end, x_start:x_end, channel] = (
            w_alpha * watermark[:, :, channel]
            + w_negative * background[y_start:y_end, x_start:x_end, channel]
        )
    return background


def create_watermark(
    text: str,
    font_face: int = FONT_HERSHEY_SIMPLEX,
    font_scale: float = 10.0,
    thickness: int = 10,
    color: tuple[int, int, int] = (255, 255, 255),
):
    size, baseline = getTextSize(text, font_face, font_scale, thickness)
    baseline += thickness
    image = zeros((size[1] + thickness, size[0], 4), uint8)
    org = (0, size[1] // 2 + baseline)
    putText(image, text, org, font_face, font_scale, color + (255,), thickness, FILLED)
    return image


def main():
    args = parse_args()
    background = imread(str(args.background))

    if (watermark_file := args.watermark) is not None:
        watermark = imread(str(watermark_file), IMREAD_UNCHANGED)
    elif (watermark_text := args.text) is not None:
        watermark = create_watermark(watermark_text)
    else:
        raise Exception("Either text or watermark image must be specified!")

    if (scale := args.scale) is not None:
        watermark = apply_scale(watermark, scale)

    if (opacity := args.opacity) is not None:
        watermark = apply_transparency(watermark, opacity)

    image = overlay(background, watermark, args.position, args.margin)

    if (output := args.output) is not None:
        print(f"Saving image as '{output}'")
        imwrite(str(output), image)

    preview_scale = 3
    image = resize(
        image, (image.shape[1] // preview_scale, image.shape[0] // preview_scale)
    )
    imshow("Image", image)
    waitKey(0)
    destroyAllWindows()


if __name__ == "__main__":
    main()
